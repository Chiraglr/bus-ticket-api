const express = require('express');
const app = express();
const assert = require('assert');
const bodyParser = require('body-parser');
const model = require('./db.js');
const f = require('util').format;
const { check, validationResult, body } = require('express-validator');
const {
    checkTicketNo,
    checkFirstName,
    checkLastName,
    checkStatus,
    checkContactNo,
    checkLandMark,
    checkState,
    checkPinCode,
    checkStreetAddress,
    checkUserName,
    checkPassword} = require('./inputValidation.js');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.get('/ticketStatus/:id',[
    checkTicketNo
    ],(req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    model.findOne({ticketNo: {$eq: parseInt(req.params.id)}}).then( doc => res.send(doc)).catch(err=> res.send(err));
});

app.get('/closedTickets',(req,res)=>{
    model.find({status: {$eq: true}}).then( collection => res.send(collection) ).catch(err=> res.send(err));
});

app.get('/openTickets',(req,res)=>{
    model.find({status: {$eq: false}}).then( collection => res.send(collection) ).catch(err=> res.send(err));
});

app.get('/passenger',[
    checkFirstName,
    checkLastName
    ],(req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    model.findOne({firstName: {$regex: req.query.firstName, $options: "i"},lastName: {$regex: req.query.lastName, $options: "i"}}).then( doc => res.send(doc)).catch(err=> res.send(err));
});

app.post('/updateTicketStatus/:id',[
    checkTicketNo,
    checkFirstName,
    checkLastName,
    checkStatus,
    checkContactNo,
    checkLandMark,
    checkState,
    checkPinCode,
    checkStreetAddress
    ],(req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty() && req.body.status!==false) {
        return res.status(422).json({ errors: errors.array() });
    }
    if(req.body.status===false){
        Object.keys(req.body).forEach(function(key,index) {
            if(key!=='ticketNo' && key!=='status')
                    req.body[key] = 'NULL';
        });
    }
    const {contactNo} = req.body;
    const ticketNo = req.params.id;
    model.
    updateOne({ticketNo: parseInt(ticketNo),
    $or:[
        {status: true, contactNo: contactNo},
        {status: false}
      ]
    }, {$set: req.body}).
    then(doc => res.send(doc)).
    catch(err=>res.send(err));
});

app.put('/adminReset',[
    checkUserName,
    checkPassword
    ],(req,res)=>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }
    var {username,password} = req.body;
    var authMechanism = 'DEFAULT';
    var url = `mongodb+srv://${username}:${password}@cluster0-yl69r.mongodb.net/BUS_TICKETS?retryWrites=true&w=majority`;
  model.adminReset(url).then(()=>res.send('db reset')).catch(err => res.send(err));
});

const port = process.env.PORT || 5000;
app.listen(port,() => console.log(`Listening on port ${port}....`));