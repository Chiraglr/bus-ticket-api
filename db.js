const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb+srv://chirag:qwerty123@cluster0-yl69r.mongodb.net/BUS_TICKETS?retryWrites=true&w=majority';
const dbName = 'BUS_TICKETS';
const collectionName = 'Tickets';
const client = new MongoClient(url, { useNewUrlParser: true }, { useUnifiedTopology: true });

const findOne = (searchQuery) => {
    return client.connect().then(()=>{
        return client.db(dbName).collection(collectionName).findOne(searchQuery).then(doc=>{
            return doc;
        }).catch(err => {return err});
    });
};

const find = (searchQuery) => {
    return client.connect().then(()=>{
        return client.db(dbName).collection(collectionName).find(searchQuery).toArray().then(collection=>{
            const response = {openTicketCount: collection.length,
            openTickets: collection};
            return response;
        }).catch(err => {return err});
    });
}

const updateOne = (searchQuery, newDoc) => {
    return client.connect().then(()=>{
        return client.db(dbName).collection(collectionName).updateOne(searchQuery,newDoc);
    });
}

const adminReset = (url) => {
    var resetDB = {"status": false, "firstName": "NULL", "lastName": "NULL", "contactNo": "NULL", "streetAddress": "NULL", "landMark": "NULL", "city": "NULL", "state": "NULL", "pincode": "NULL"};
    var client = new MongoClient(url, { useNewUrlParser: true }, { useUnifiedTopology: true });
    return client.connect().then(()=>{
        return client.db(dbName).collection(collectionName).update({status: true},{$set: resetDB});
    });
}

module.exports = { findOne, find, updateOne, adminReset };